const {modulesRaw} = require("./modules")

const modules=modulesRaw.split('\n')
let sum=modules.reduce(function(acc, next){
    let fuelCost = 0;
    let currentFuel = next;
    while(true){
        const currentCost = Math.floor(Number(currentFuel)/3)-2;
        if(currentCost>0){
            currentFuel=currentCost;
            fuelCost+=currentCost;
        }
        else{
            break;
        }
    }
    return acc+fuelCost;
}, 0)

console.log(sum)